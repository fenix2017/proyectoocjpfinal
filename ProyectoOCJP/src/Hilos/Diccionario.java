package Hilos;

import javax.swing.JOptionPane;

public class Diccionario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String cadena[]=new String[50];
		int i=0, tamano;
		boolean salir=false;
		System.out.println("******************Diccionario******************");
		do
		{
			cadena[i]=JOptionPane.showInputDialog("Ingrese palabra o comando a buscar");
			if(!(cadena[i].equals("Fin"))&&!(cadena[i].equals("fin"))&&!(cadena[i].equals("FIN")))
			{
				salir=false;
			}	
			else
			{
				salir=true;
			}
			i++;
		}
		while(salir==false);
		tamano=i-2;
		
		
		LeerPalabra leerP = new LeerPalabra(cadena, tamano);
		leerP.start();
	}

}
