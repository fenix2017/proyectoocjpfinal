package Hilos;

public class ResponderConsulta extends Thread {
	String comando;
	String nombreHilo;
	String[] diccionarioComandos={
			"cat","concatena archivos",
			"cd","cambia de directorio",
			"chmod","cambia los permisos de un archivo",
			"cp","copia archivos",
			"diff","encuetra diferencia entre dos archivos",
			"file","Muestra el tipo de un archivo",
			"find","Encuentra archivos",
			"grep","Busca patrones en archivos",
			"head","Muestra el inicio de un archivo",
			"mkdir","Crea un directorio",
			"less","Visualiza p�gina a p�gina un archivo",
			"ls","Lista el contenido del directorio ",
			"pwd","Muestra la ruta del directorio actual",
			"rm","borra un fichero",
			"echo","Escribe mensaje en la salida est�ndar",
			"kill","matar un proceso",
			"man","Ayuda del comando especificado",
			"passwd","Cambia la contrasenia",
			"ps","Muestra informacion de los procesos",
			"who","muestra informacion de los usuarios",
			"bc","Calculadora y lenguaje matem�tico, muy potente",
			"cal","Despliega un calendario.",
			"cfdisk","Herramienta de particionamiento de discos, usada en sistemas debian principalmente",
			"chroot","Ejecuta comandos de root en un shell restringido a un directorio y sus subdirectorios",
			"clear","limpia la terminal",
			"date","Muestra/establece la fecha y hora actual",
			"du","Muestra el uso de espacio de archivos y directorios",
			"exit","Sale del shell o terminal actua",
			"gcc","Compilador de C y de C++ de GNU",
			"groupadd","Crea un nuevo grupo en el sistema",
			"group","Imprime los grupos a los que pertenece un usuario",
			"help","Ayuda sobre los comandos internos de bash",
			"history","Muestra el historial de comandos del usuario",
			"hostname","Despliega el nombre del equipo",
			"jobs","Muestra los trabajos del usuario en suspensi�n o en background",
			"netstat","Herramienta de red que muestra conexiones, tablas de ruteo, estad�sticas de interfaces, etc",
			"ping","Manda un echo_request (solicitud de eco) a un equipo en al red",
			"pstree","Muestra los procesos en forma de �rbol",
			"reboot","Reinicia el equipo",
			"route","Muestra/altera la tabla de ruteo IP",
			"screen","Administrador de terminales virtuales",
			"service","Ejecuta/detiene servicios en modo manual",
			"set","Muestra o establece el entorno de variables para el usuario actuual.",
			"ssh","Programa de login remoto seguro, programa del paquete openssh (protocolo de comunicaci�n encriptado)",
			"sudo","Permite indicar que usuario ejecuta que comandos de root",
			"top","Muestra los procesos del sistema de manera interactiva y continua.",
			"touch","Crea archivos vacios, cambia fechas de acceso y/o modificaci�n de archivos",
			"uname","Despliega informaci�n del sistema",
			"vmstat","Proporciona informaci�n sobre la memoria virtual",
			"wget","Descargador de archivos desde Internet, no interactivo",
	};
	
	public ResponderConsulta(String cadena, String cadena1)
	{
		this.comando=cadena;
		this.nombreHilo=cadena1;
	}

	public String getNombreHilo() {
		return nombreHilo;
	}

	public void setNombreHilo(String nombreHilo) {
		this.nombreHilo = nombreHilo;
	}

	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}
	
	public void run()
	{
		int i=0;
		do
		{
			System.out.println(this.nombreHilo+":\tBuscando comando "+this.comando);
			if(this.comando.equals(diccionarioComandos[i]))
			{
				//System.out.println("Comando encontrado ");
				//System.out.println("Comando\t\t\tDefinicion");
				System.out.println(this.comando+"\t\t\t"+diccionarioComandos[i+1]);
				i=diccionarioComandos.length+1;
			}
			else
			{
				//System.out.println("no se encuentra"+" comado actual: "+diccionarioComandos[i]+"\ti:"+i);
				i=i+2;
			}
			
			try
			{
				Thread.sleep(500);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		//this.comando.equals(diccionarioComandos[i]))
		while((i<=diccionarioComandos.length-2));
		
		if(i==diccionarioComandos.length)
		{
			System.out.println("Comando no encontrado");
		}
	}

}
