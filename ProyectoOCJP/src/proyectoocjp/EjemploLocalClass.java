/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author Raul Molina
 */

public class EjemploLocalClass {
  
    static String expresionRegular = "[^0-9]";
  
    public static void validarNumeroTelefono(
        
        String telefono1, String telefono2) {
      
        final int tamanioNumero = 10;
                      
        class numeroTelefono {
            
            String numeroTelefonoFormateado = null;

            numeroTelefono(String numeroTelefono){
                // numberLength = 7;
                String numeroActual = numeroTelefono.replaceAll(expresionRegular, "");
                
                if (numeroActual.length() == tamanioNumero)
                    numeroTelefonoFormateado = numeroActual;
                else
                    numeroTelefonoFormateado = null;
            }

            public String getNumber() {
                return numeroTelefonoFormateado;
            }
            

        }

        numeroTelefono num1 = new numeroTelefono(telefono1);
        numeroTelefono num2 = new numeroTelefono(telefono2);
        

        if (num1.getNumber() == null) 
            System.out.println("Primer numero Invalido");
        else
            System.out.println("Primer numero es:" + num1.getNumber());
        if (num2.getNumber() == null)
            System.out.println("Segundo numero invalido");
        else
            System.out.println("Segundo numero es: " + num2.getNumber());

    }
}