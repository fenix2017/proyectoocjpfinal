/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author molin
 */
class EjemploStaticClassOuter
{  
    static int datos=30;  
    private int datos2=3; //Este dato no puede ser accedido en la clase inner
    
    static class Inner
    {  
        void mensaje()
        {
            System.out.println("El dato es: "+datos);
            //System.out.println("Dato 2:"+datos2); No se puede accedaer a datos que no sean static
        }  
    }    
}
