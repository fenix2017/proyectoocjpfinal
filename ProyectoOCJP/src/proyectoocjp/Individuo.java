/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author Buhobit
 */
public interface Individuo {
    //No se crean atributos a menos que sea static final
    
    public static final float gravedad = 9.82f;
    //Sus metodos no se implementan
    public void viaja();
    public void alimenta();
}
