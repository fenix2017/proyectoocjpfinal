/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author Bryan
 */
import java.util.Random;
import java.lang.Math;

public class Assert {

    /**
     * @param args the command line arguments
     */
    private Random random;
    
    private float ladoa;
    private float ladob;
    private float ladoc;
    
    public Assert()
    {
        random = new Random();
    }
    
    public Assert(float ladoa, float ladob, float ladoc){
        this.ladoa = ladoa;
        this.ladob = ladob;
        this.ladoc = ladoc;
    }
    
    public int NumeroSiguiente() {
		int i = random.nextInt(40);
		assert i >= 0 && i < 10: String.format("El número devuelto no cumple la postcondición (%d):", i);		
		return i;
    }
    
    public boolean esRectangulo(){
        //Sabemos un lado de un triangulo debe ser menor que la suma
        //de los otros dos lados.
        assert(ladoa < ladob + ladoc): "\nladoa: " + ladoa
                                     + " no es < que:  "
                                     + ladob + " + " + ladoc;
        return(Math.pow(ladoc,2) == Math.pow(ladoa,2) + Math.pow(ladob,2));
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Assert pp = new Assert();
        System.out.println(pp.NumeroSiguiente());
        
        EjemploBasicoAssert();
        
        Assert triangulo = new Assert(10,3,2);
        if (triangulo.esRectangulo()){
           System.out.println("El triangulo t2 es rectangulo");
        }
    }

    private static void EjemploBasicoAssert() {
        int x = -15;
        assert x > 0 : "El valor debe ser positivo";
        System.out.println("Valor positivo x: " + x);
    }
    
}
