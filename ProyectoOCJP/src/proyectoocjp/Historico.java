/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;
/**
 *
 * @author Bryan
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Historico {
    
    protected static int numeroDeCampeonatos = 0;
    
    public static void main(String[] args) throws IndexOutOfBoundsException, IOException {
        // TODO code application logic here
        
        BufferedReader  br =    new BufferedReader(new InputStreamReader(System.in));
        
        String Equipo1 = "Delfin";
        String Equipo2 = "Independiente";
        String Equipo3 = "Barcelona";
        String Equipo4 = "Emelec";
        String Equipo5 = "Liga de quito";
        
        boolean fueComprobadoDelfin= false;
        boolean fueComprobadoInd= false;
        boolean fueComprobadoBar= false;
        boolean fueComprobadoEme= false;
        boolean fueComprobadoLig= false;
        
        
        try{
        while(true){
            
                //preguntar e inicializar
                //solo los equipos de primera A
                System.out.println("Ingrese al equipo que quiere agregar campeonatos nacionales: ");
                System.out.println("Los equipos posibles son: " + Equipo1 + " " +Equipo2+ " " +
                        Equipo3 + " " + Equipo4 + " " + Equipo5);
                String sr = br.readLine();
                //int res = Integer.parseInt(sr);

            if(sr.equals(Equipo1))
            {
                if(fueComprobadoDelfin){
                    System.out.println("Equipo ya revisado");
                    continue;}
                int num=0;

                System.out.println("Ingresar el numero de titulos del: " + Equipo1);
                String del = br.readLine();
                int delf = Integer.parseInt(del);
                System.out.println("El " + Equipo1 + " tiene " + delf + " titulos ");

                if(delf == 0 || del == "cero"){fueComprobadoDelfin = true; continue;  }

            }
            if (sr.equals(Equipo2) )
            {
                if(fueComprobadoInd){System.out.println("Equipo ya revisado");
                    continue;}
             int num=0;
                System.out.println("Ingresar el numero de titulos del:" + Equipo2);
                String ind = br.readLine();
                int inde = Integer.parseInt(ind);
                System.out.println("El " + Equipo2 + " tiene " + inde + " titulos"); 
                if(inde == 0 || ind == "cero"){ fueComprobadoInd = true; continue;}
            }
            if (sr.equals(Equipo3) )
            {
                if(fueComprobadoBar){System.out.println("Equipo ya revisado");
                    continue;}
             int num=0;
                System.out.println("Ingresar el numero de titulos del:" + Equipo3);
                String bar = br.readLine();
                int barc = Integer.parseInt(bar);
                System.out.println("El " + Equipo3 + " tiene " + barc + " titulos");
                if(barc > 0 || bar != "cero"){ fueComprobadoBar =true;  continue;}
            }
            if (sr.equals(Equipo4))
            {
                if(fueComprobadoEme){System.out.println("Equipo ya revisado");
                    continue;}
             int num=0;
                System.out.println("Ingresar el numero de titulos del:" + Equipo4);
                String eme = br.readLine();
                int emel = Integer.parseInt(eme);
                System.out.println("El " + Equipo4 + "tiene" + emel + "titulos");   
                if(emel > 0 || eme != "cero"){ fueComprobadoEme =true; continue;}
            }
            if (sr.equals(Equipo5))
            {
                if(fueComprobadoLig){System.out.println("Equipo ya revisado");
                    continue;}
             int num=0;
                System.out.println("Ingresar el numero de titulos del:" + Equipo5);
                String lig = br.readLine();
                int ligd = Integer.parseInt(lig);
                System.out.println("El " + Equipo5 + "tiene" + ligd + "titulos"); 
                if(ligd > 0 || lig != "cero"){ fueComprobadoLig =true; continue;}
            }       

            while(fueComprobadoDelfin == true && fueComprobadoInd == true && fueComprobadoBar == true &&
                  fueComprobadoEme == true && fueComprobadoLig == true)
            {
                numeroDeCampeonatos = 1;
                break;
            }
                //break;
            }

        }catch(Exception e){
            System.out.println(""+e.getMessage());
        }
        
        
    }
    
}
