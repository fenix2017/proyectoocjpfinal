/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author root
 */
public class FechaHoraPartido {
    
    HashMap<String,Object> hashmap;

    public HashMap fechaHoraPartido() {
        String str1 = "16";
	String str2 = "57";
	String str3 = "3.33";
        Integer wi1 = new Integer(str1);
	Long wl1 = new Long(str2);
	Float wf1 = new Float(str3);
        hashmap.put("Hora", wi1);
        hashmap.put("Mes", wl1);
        hashmap.put("Minuto", wf1);
        
        return hashmap;
    }
    
    public String miFormatoActual(){
        
        String [] datos ={fechaHoraPartido().get("Hora").toString(),fechaHoraPartido().get("Mes").toString(),fechaHoraPartido().get("Minuto").toString()};
        String hora1= String.join("-", datos);
        
        return hora1;
    }
    
       // ConvSegundosertir fecha 02-05-2016 02/04/2016 2016/04/26 => 2017-05-05
    public String miFormatoYYMMDD(String fecha_partido){
        
        Date date = new Date();
        
        //Caso 1: obtener la hora y salida por pantalla con formato:
        String format= new String("HH:mm:ss");
        
        DateFormat hourFormat = new SimpleDateFormat(format.substring(0, 3));
        System.out.println("Hora: "+hourFormat.format(date));

        //convierte la fecha DD/MM/YY a DD-MM-YY y la guarda en un arreglo
        String[] fecha = fecha_partido.replace('/', '-').split("-");
        String DIA=fecha[0];
        String MES=fecha[1];
        String ANIO=fecha[2];
        //Pero si la Fecha partido es de la forma 2017-08-05
        if(fecha[0].length()==4) {
            ANIO=fecha[0];
            DIA=fecha[2];
        }
        return ANIO +"-"+ MES +"-"+ DIA;
    }
}
