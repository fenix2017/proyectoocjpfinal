/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;
/**
 *
 * @author Buhobit
 */
public class ProyectoOCJP {
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        double prob;
        FechaHoraPartido fechaHora= new FechaHoraPartido();
        System.out.println(""+fechaHora.miFormatoActual());
        System.out.println(""+fechaHora.miFormatoYYMMDD("2016/06/17"));
        
        Partido partido1 = new Partido();
        
        partido1.equipoA();
        
        /*******  VS ******/
        partido1.equipoB();
        
        Pronostico pronostico=new Pronostico();
        prob=pronostico.probabilidadDeGanar(Equipo.BARCELONA.getPosicionTabla(),Equipo.LIGA_DE_QUITO.getPosicionTabla());
        
        System.out.println("La probabilidad de Ganar es:"+prob);
        
        pronostico.publico();
        
        System.out.println("Ejemplo Inner Class");
        EstructuraDatos ds = new EstructuraDatos();
        ds.imprimirPares();
        
        System.out.println("Ejemplo TreeSet");
        EjemploTreeSet ts = new EjemploTreeSet();
        int vector[]={1,2,3,4,5};
        ts.treeSet(vector);
        
        System.out.println("Ejemplo Static Nested Class");
        EjemploStaticClassOuter.Inner obj=new EjemploStaticClassOuter.Inner();  
        obj.mensaje();  
        
        System.out.println("Ejemplo Local Class");
        EjemploLocalClass.validarNumeroTelefono("123-456-7890", "456-asa-1232");
      
        
    }
    
}
