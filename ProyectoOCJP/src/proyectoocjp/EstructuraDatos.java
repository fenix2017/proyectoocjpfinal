/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author molin
 */
public class EstructuraDatos {
    
    // Create an array
    private final static int SIZE = 15;
    private int[] arrayEnteros = new int[SIZE];
    
    public EstructuraDatos() 
    {
        // fill the array with ascending integer values
        for (int i = 0; i < SIZE; i++) {
            arrayEnteros[i] = i;
        }
    }
    
    public void imprimirPares() 
    {    
        // Imprime los valores pares del array
        DataStructureIterator iterator = this.new IteradorPares();
        
        while (iterator.hasNext()) 
        {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }
    
    interface DataStructureIterator extends java.util.Iterator<Integer> { } 

    // Inner class implementa la DataStructureIterator interface,
    // la cual hereda de the Iterator<Integer> interface
    
    private class IteradorPares implements DataStructureIterator {
        
        // Start stepping through the array from the beginning
        private int nextIndex = 0;
        
        public boolean hasNext() 
        {
            
            // Verifica si el elemento actual es el ultimo del array
            return (nextIndex <= SIZE - 1);
        }        
        
        public Integer next() 
        {            
            // Guarda el valor de un indice par del array
            Integer valorIndice = Integer.valueOf(arrayEnteros[nextIndex]);
            // Get the next even element
            nextIndex += 2;
            return valorIndice;
        }
    }
}