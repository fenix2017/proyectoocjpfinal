/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;
/**
 *
 * @author molin
 */
public class EjemploTreeSet {

    public void treeSet(int valores[]) 
    {
        Scanner in = new Scanner(System.in);
        int numero=0;
	System.out.println("Ejemplo TreeSet!\n");
	TreeSet<Integer> tree = new TreeSet<Integer>();
        for(int i=0;i<valores.length;i++)
        {
            tree.add(valores[i]);
        }
 
	// TreeSet ordena los elemntos del vector recibido
	Iterator<Integer> iterator = tree.iterator();
	System.out.print("Datos en el TreeSet: ");
 
	// Mostrando los datos en el TreeSet
        
	while (iterator.hasNext()) 
        {
		System.out.print(iterator.next() + " ");
	}
        
	System.out.println();
 
	// Verifica si el TreeSet esta Vacio
	if (tree.isEmpty()) 
        {
		System.out.print("Tree Set is empty.");
	} 
        else 
        {
		System.out.println("Tree Set size: " + tree.size());
	}
 
	// Devuelve el primer elemento del TreeSet
	System.out.println("Primer Dato: " + tree.first());
 
	// Devuelve el ultimo dato del TreeSet
	System.out.println("Ultimo Dato: " + tree.last());
 
        System.out.println("Ingrese un nuemero a eliminar del TreeSet:");
        numero=in.nextInt();
	
        if (tree.remove(numero)) 
        { // elimina el elemento del TreeSet
		System.out.println("Dato eliminado del TreeSet");
	} 
        else 
        {
		System.out.println("El dato ingresado no existe en el TreeSet");
	}
        
	System.out.print("Elementos actuales del TreeSet: ");
	iterator = tree.iterator();
 
	// Elementos del TreeSet actualizado
	while (iterator.hasNext()) 
        {
		System.out.print(iterator.next() + " ");
	}
	System.out.println();
	
        System.out.println("El tamanio del TreeSet despues de la eliminacion es: " + tree.size());
 
	// Eliminamos todos los elementos del TreeSet
	tree.clear();
        
	if (tree.isEmpty()) 
        {
		System.out.print("El TreeSet esta vacio");
	} 
        else 
        {
		System.out.println("El tamanio del TreeSet es: " + tree.size());
	}
   }
    
}
