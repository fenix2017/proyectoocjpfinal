/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author molin
 */
public enum Equipo {
    //Enum es una clase en el que existe solo estos objetos
    //Equipos Serie A, 
    	DELFIN("Delfin SC",1),INDEPENDIENTE("Independiente del Valle",2),BARCELONA("Barcelona SC",3), 
        EMELEC("Emelec SC",4),LIGA_DE_QUITO("Liga de Quito",11) ; 
	
	private String nombreCLUB;
	private int posicionTabla;

    private Equipo() {
    }
	//Enum tiene su consructor privado
	private Equipo (String nombreClub, int puestoLiga){
		this.nombreCLUB = nombreClub;
		this.posicionTabla = puestoLiga;
	}

        public String getNombreCLUB() {
            return nombreCLUB;
        }

        public int getPosicionTabla() {
            return posicionTabla;
        }

        public void setPosicionTabla(int posicionTabla) {
        this.posicionTabla = posicionTabla;
        }

    @Override
    public String toString() {
        String salida ="";
        
        salida+=this.nombreCLUB+" - Posicion: "+this.posicionTabla;
        System.out.println(""+salida);
        return salida;
        
    }
}
