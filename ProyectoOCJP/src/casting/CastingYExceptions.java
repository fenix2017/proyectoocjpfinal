/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package casting;

/**
 *
 * @author Buhobit
 */
public class CastingYExceptions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
   
    Object objeto = new Object();
    Fruta fruta = new Fruta();
    Manzana manzana = new Manzana();
    Citrico citrico = new Citrico();
    Orange naranja = new Orange();
    Squeezable exprimible = null;
   
    System.out.println("=====Cheking cast for FruitFruit=======");
    try{
    //fruta = (Manzana)fruta;
    
    fruta = (Fruta)manzana;
    System.out.println("Apple: ok");
   }catch (ClassCastException ce){
       System.out.println("Apple: NO");   
   }
    
    try{
    fruta = (Fruta)(Squeezable)fruta;
    
       System.out.println("Squeezable: ok");
   }catch (ClassCastException ce){
       System.out.println("Squeezable: NO ");   
   }
    
   try{
    fruta = (Fruta)(Citrico)fruta;
       System.out.println("Citrus: ok");
   }catch (ClassCastException ce){
       System.out.println("Citrus: NO");   
   }
   
   try{
    fruta = (Fruta)(Orange)fruta;
       System.out.println("Orange: ok");
   }catch (ClassCastException ce){
       System.out.println("Orange: NO");   
   }    
    
        System.out.println("=====Cheking cast for appleFruit=======");
   //==============================
       
   
   try{
    manzana = (Manzana)(Fruta)manzana;
    System.out.println("Apple: ok");
   }catch (ClassCastException ce){
       System.out.println("Apple: NO");   
   }
   
   try{
    manzana = (Manzana)(Fruta)exprimible;
       System.out.println("Squeezable: ok");
   }catch (ClassCastException ce){
       System.out.println("Squeezable: NO ");   
   }
   
   try{
    manzana = (Manzana)(Fruta)citrico;
       System.out.println("Citrus: ok");
   }catch (ClassCastException ce){
       System.out.println("Citrus: NO");   
   }
   
   try{
    manzana = (Manzana)(Fruta)naranja;
       System.out.println("Naranja: ok");
   }catch (ClassCastException ce){
       System.out.println("Naranja: NO ");   
   }
   
   
   
    }
    
}
